// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeBase.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(1.1);
	FVector WallOffset(ForceInitToZero);
	WallOffset.X = FMath::FRandRange(-20, 20);
	WallOffset.Y = FMath::FRandRange(-20, 20);
	AddActorWorldOffset(WallOffset);
	FRotator WallRotation(ForceInitToZero);
	WallRotation.Yaw = FMath::FRandRange(-60, 60);
	AddActorWorldRotation(WallRotation);
}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}

