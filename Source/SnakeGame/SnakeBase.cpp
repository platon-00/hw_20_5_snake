// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "cmath"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 7.f;
	LastMoveDirection = EMovementDirection::UP;
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();	
	
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{	
	if (ElementsNum == 1 && SnakeElements.Num() != 0)
	{
		auto LastElem = SnakeElements[SnakeElements.Num() - 1];
		//FVector NewLocation(LastElem->GetActorLocation().X, LastElem->GetActorLocation().Y, LastElem->GetActorLocation().Z);
		FTransform NewTransform(LastElem->GetActorLocation());
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		//MovementSpeed *= 1.005;// �������� ������ ����������� � ������ ����� ������
		SetActorTickInterval(MovementSpeed);
	}
	else
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(-(SnakeElements.Num() * ElementSize), 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	} 
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;


	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
}
	//
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	auto HeadLocation = SnakeElements[0]->GetActorLocation();
	if (abs(HeadLocation.X) > 5 * ElementSize) {
		FVector NewHeadLocation;
		NewHeadLocation.X = -HeadLocation.X;
		NewHeadLocation.Y = HeadLocation.Y;
		NewHeadLocation.Z = HeadLocation.Z;
		SnakeElements[0]->SetActorLocation(NewHeadLocation);
		SnakeElements[0]->AddActorWorldOffset(MovementVector);
	}
	if (abs(HeadLocation.Y) > 11 * ElementSize) {
		FVector NewHeadLocation;
		NewHeadLocation.X = HeadLocation.X;
		NewHeadLocation.Y = -HeadLocation.Y;
		NewHeadLocation.Z = HeadLocation.Z;
		SnakeElements[0]->SetActorLocation(NewHeadLocation);
		SnakeElements[0]->AddActorWorldOffset(MovementVector);
	}


	SnakeElements[0]->ToggleCollision();
	InputExist = false;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{	
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

