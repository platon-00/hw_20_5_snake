// Fill out your copyright notice in the Description page of Project Settings.



#include "Kismet/GameplayStatics.h"
#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//FoodMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FoodMeshComponent"));
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	FVector NewFoodLocation;
	NewFoodLocation.X = FMath::FRandRange(-300, 300);
	NewFoodLocation.Y = FMath::FRandRange(-300, 300);
	NewFoodLocation.Z = 0;
	SetActorLocation(NewFoodLocation);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead) 
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			FoodNumb++;
			if (FoodNumb > 4) {
				FName LevelAssetPath = TEXT("Level_2");
				UGameplayStatics::OpenLevel(this, LevelAssetPath, true);
			}
			Snake->AddSnakeElement();
			FVector NewFoodLocation;
			NewFoodLocation.X = FMath::FRandRange(-300, 300);
			NewFoodLocation.Y = FMath::FRandRange(-300, 300);
			NewFoodLocation.Z = 0;
			//FTransform NewFoodTransform(NewFoodLocation);
			//AFood* NewFood = GetWorld()->SpawnActor<AFood>(MyFoodClass, NewFoodTransform);
			
			SetActorLocation(NewFoodLocation);
		}
	}
}